package Automation.Automation;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Pages.confirmationPage;
import Pages.formAutomation;

public class AppTest 
{
	WebDriver driver;
	formAutomation form;
	confirmationPage conPage;
	
	@Before
	public void setup()
    {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Altamirano PC\\Documents\\Browser Drivers\\chromedriver.exe");
    	driver = new ChromeDriver();
    }
	
    @Test
    public void testFormFill()
    {
    	driver.get("https://formy-project.herokuapp.com/form");
        form = new formAutomation(driver);
        
        form.setFirstName().sendKeys("Diana");
        form.setLastName().sendKeys("Altamirano");
        form.setJob().sendKeys("Developer");
        form.setEducation().click();
		form.setSex().click();
		form.setMenu().click();
		form.setOption().click();
		form.setDate().sendKeys("06/18/1998");
		form.setSubmit().click();
		
    }
    
    @After 
    public void testSubmitMsj() {
    	conPage = new confirmationPage(driver);
    	
    	WebDriverWait wait = new WebDriverWait(driver, 10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(conPage.setSubmitMsj()));
    	Assert.assertEquals(conPage.setThanksMsj().getText(), "Thanks for submitting your form");
    }
    
    
}
