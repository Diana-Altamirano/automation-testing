package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class confirmationPage {
	
	protected WebDriver driver;
	
	By submitMsj = By.xpath("/html/body/div/div");
	
	@FindBy(xpath=("/html/body/div/h1")) WebElement thanksMsj;
	
	public confirmationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement setThanksMsj() {
		return thanksMsj;
	}
	
	public By setSubmitMsj() {
		return submitMsj;
	}

}
