package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class formAutomation {
	
	WebDriver driver;
	
	@FindBy(id=("first-name")) WebElement firstName;
	@FindBy(id=("last-name")) WebElement lastName;
	@FindBy(id=("job-title")) WebElement job;
	@FindBy(id=("radio-button-2")) WebElement education;
	@FindBy(id=("checkbox-2")) WebElement sex;
	@FindBy(id=("select-menu")) WebElement menu;
	@FindBy(css=("#select-menu > option:nth-child(2)")) WebElement option;
	@FindBy(id=("datepicker")) WebElement date;
	@FindBy(linkText=("Submit")) WebElement submit;

	
	//private By option = By.cssSelector("#select-menu > option:nth-child(2)");
	
	
		
	
	public formAutomation(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement setFirstName() {
		return firstName;
	}
	public WebElement setLastName() {
		return lastName;
	}
	public WebElement setJob() {
		return job;
	}
	public WebElement setEducation() {
		return education;
	}
	public WebElement setSex() {
		return sex;
	}
	public WebElement setMenu() {
		return menu;
	}
	public WebElement setOption() {
		return option;
	}
	public WebElement setDate() {
		return date;
	}
	public WebElement setSubmit() {
		return submit;
	}

}
