package Automation.Automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class App 
{
    public static void main( String[] args )
    {
    	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Altamirano PC\\Documents\\Browser Drivers\\chromedriver.exe");
		
    	WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://formy-project.herokuapp.com/form");
		
		WebElement firstName = driver.findElement(By.id("first-name"));
		firstName.sendKeys("Diana");
		
		WebElement lastName = driver.findElement(By.id("last-name"));
		lastName.sendKeys("Altamirano");
		
		WebElement job = driver.findElement(By.id("job-title"));
		job.sendKeys("Developer");
		
		WebElement education = driver.findElement(By.id("radio-button-2"));
		education.click();
		
		WebElement sex = driver.findElement(By.id("checkbox-2"));
		sex.click();
		
		WebElement menu = driver.findElement(By.id("select-menu"));
		menu.click();
		
		WebElement option = driver.findElement(By.cssSelector("#select-menu > option:nth-child(2)"));
		option.click();
		
		WebElement date = driver.findElement(By.id("datepicker"));
		date.sendKeys("06/18/1998");
		
		
    }
}
