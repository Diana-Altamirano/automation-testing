package Automation.Automation;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.excel.lib.util.Xls_Reader;

import Pages.confirmationPage;
import Pages.formAutomation;

public class AppTest 
{
	WebDriver driver;
	formAutomation form;
	confirmationPage conPage;

	Xls_Reader reader = new Xls_Reader("./src/main/java/com/excel/lib/util/EmployeeData.xlsx");
	String sheetName = "Employees";
	
	@Before
	public void setup()
    {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Altamirano PC\\Documents\\Browser Drivers\\chromedriver.exe");
    	driver = new ChromeDriver();	
    }
	
    @Test
    public void testFormFill()
    {
    	
        form = new formAutomation(driver);
        
        int numRows = reader.getRowCount(sheetName);
        
        for(int i=2; i<=numRows; i++) {
        	driver.get("https://formy-project.herokuapp.com/form");

        	String name = reader.getCellData(sheetName, 0, i);
        	String lastName = reader.getCellData(sheetName, 1, i);
        	String job = reader.getCellData(sheetName, 2, 2);
        	String rbOption = reader.getCellData(sheetName, 3, i);
        	String sex = reader.getCellData(sheetName, 4, i);
        	String option = reader.getCellData(sheetName, 5, i);
        	String date = reader.getCellData(sheetName, 6, i);
            
            form.setFirstName().sendKeys(name);
            form.setLastName().sendKeys(lastName);
            form.setJob().sendKeys(job);
            form.setEducation(rbOption);
    		form.setSex(sex);
    		form.setMenu().click();
    		form.setOption(option);
    		form.setDate().sendKeys(date);
        }
        
		form.setSubmit().click();
		
    }
    
    @After 
    public void testSubmitMsj() {
    	conPage = new confirmationPage(driver);
    	
    	WebDriverWait wait = new WebDriverWait(driver, 10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(conPage.setSubmitMsj()));
    	Assert.assertEquals(conPage.setThanksMsj().getText(), "Thanks for submitting your form");
    }
    
    
}
