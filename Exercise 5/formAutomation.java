package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class formAutomation {
	
	WebDriver driver;
	
	@FindBy(id=("first-name")) WebElement firstName;
	@FindBy(id=("last-name")) WebElement lastName;
	@FindBy(id=("job-title")) WebElement job;
	@FindBy(id=("radio-button-1")) WebElement educationOp1;
	@FindBy(id=("radio-button-2")) WebElement educationOp2;
	@FindBy(id=("radio-button-3")) WebElement educationOp3;
	@FindBy(id=("checkbox-1")) WebElement sexOp1;
	@FindBy(id=("checkbox-2")) WebElement sexOp2;
	@FindBy(id=("checkbox-3")) WebElement sexOp3;
	@FindBy(id=("select-menu")) WebElement menu;
	@FindBy(css=("#select-menu > option:nth-child(2)")) WebElement option1;
	@FindBy(css=("#select-menu > option:nth-child(3)")) WebElement option2;
	@FindBy(css=("#select-menu > option:nth-child(4)")) WebElement option3;
	@FindBy(css=("#select-menu > option:nth-child(5)")) WebElement option4;
	@FindBy(id=("datepicker")) WebElement date;
	@FindBy(linkText=("Submit")) WebElement submit;

	
	public formAutomation(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement setFirstName() {
		return firstName;
	}
	public WebElement setLastName() {
		return lastName;
	}
	public WebElement setJob() {
		return job;
	}
	public void setEducation(String options) {
		switch(options) {
		case "High":
			educationOp1.click();
			break;
		case "College":
			educationOp2.click();
			break;
		case "Grad":
			educationOp3.click();
			break;
		}
	}
	public void setSex(String options) {
		switch(options) {
		case "M":
			sexOp1.click();
			break;
		case "F":
			sexOp2.click();
			break;
		case "P":
			sexOp3.click();
			break;
		}
	}
	
	public WebElement setMenu() {
		return menu;
	}
	public void setOption(String options) {
		switch(options) {
		case "1.0": 
			option1.click();
			break;
		case "2.0": 
			option2.click();
			break;
		case "3.0": 
			option3.click();
			break;
		case "4.0": 
			option4.click();
			break;
		}
	}
	public WebElement setDate() {
		return date;
	}
	public WebElement setSubmit() {
		return submit;
	}

}
